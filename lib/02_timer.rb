class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours, minutes, seconds = padded(self.convert_seconds)
    "#{hours}:#{minutes}:#{seconds}"
  end

  def convert_seconds
    seconds = @seconds

    minutes = seconds / 60
    seconds = seconds % 60
    hours = minutes / 60
    minutes = minutes % 60
    [hours, minutes, seconds]
  end

  def padded(arr)
    arr.map {|val| val < 10 ? val = "0#{val}" : val }
  end
end
