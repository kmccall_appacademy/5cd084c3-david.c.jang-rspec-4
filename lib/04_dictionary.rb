class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries
  def initialize
     @entries = {}
     @keywords = []
  end

  def add(input)
     if input.class == String
         @entries[input] = nil
     elsif input.class == Hash
         @entries.merge!(input)
     end
  end

  def keywords
      @entries.keys.sort
  end

  def include?(keyword)
     @entries.include?(keyword)
  end

  def find(string)
     @entries.select {|k,v| k.include?(string)}
  end

  def printable
    ans = keywords.map { |key| %Q{[#{key}] "#{@entries[key]}"} }
    ans.join("\n")

  end
end
