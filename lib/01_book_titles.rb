class Book
  # TODO: your code goes here!

  def initialize

  end

  def title
    @title
  end

  def title=(name)
    @title = correctly_capitalize(name)
  end

  def correctly_capitalize(book_name)
    not_to_capitalize = ["and","the","in","of","a","an"]
    ans = book_name.split(" ").map.with_index do |word, indx|
      if ! not_to_capitalize.include?(word)
        word.capitalize
      else
        indx == 0 ? word.capitalize : word
      end

    end
    ans.join(" ")
  end

end
