class Temperature
  attr_accessor :f, :c

  def initialize(hash)
    if hash[:f]
      @f = hash[:f]
    else
      @c = hash[:c]
    end

  end

  def in_fahrenheit
     self.c.nil? ? self.f : (self.c * 9 / 5.0) + 32
  end

  def in_celsius
     self.f.nil? ? self.c : (self.f - 32) * (5 / 9.0)
  end


  # factory method returns a new object of the class
  def self.from_celsius(celsius)
    return Temperature.new(:c => celsius)
  end
  # factory method returns a new object of the class
  def self.from_fahrenheit(fahrenheit)
    return Temperature.new(:f => fahrenheit)
  end

  # class methods
  def self.ctof(temp)
    (temp * 9 / 5.0) + 32
  end

  def self.ftoc(temp)
    (temp - 32) * (5 / 9.0)
  end
end

class Celsius < Temperature
  attr_accessor :c
  def initialize(celsius_temperature)
    @c = celsius_temperature
  end
end

class Fahrenheit < Temperature
  attr_accessor :f
  def initialize(fahrenheit_temperature)
    @f = fahrenheit_temperature
  end
end
